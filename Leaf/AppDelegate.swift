//
//  AppDelegate.swift
//  Leaf
//
//  Created by Martin Elvar on 23/09/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //Load Flurry
        Flurry.setCrashReportingEnabled(true)
        Flurry.startSession("RD55Q7ZNVJ59R7HH2ZS3")
        
        //replace and push rootview manually
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UIViewController
        self.window?.makeKeyAndVisible()
        
        Flurry.logAllPageViewsForTarget(self.window?.rootViewController)
        
        /*  // THIS IS MUCH NEEDED LATER - IT LOADS STORYBOARDS BASED ON PHONE MODEL
            // REMEMBER TO RENAME Main.storyboard to "Storyboard_iPhone_5"
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
            var iOSDeviceScreenSize = UIScreen.mainScreen().bounds.size
            var initialViewController: UIViewController?
            
            if iOSDeviceScreenSize.height == 480 {
                var iPhone35InchStoryBoard = UIStoryboard(name: "Storyboard_iPhone_3-4", bundle: nil)
                initialViewController = iPhone35InchStoryBoard.instantiateInitialViewController() as? UIViewController
            }
            if iOSDeviceScreenSize.height == 568 {
                var iPhone4InchStoryBoard = UIStoryboard(name: "Storyboard_iPhone_5", bundle: nil)
                initialViewController = iPhone4InchStoryBoard.instantiateInitialViewController() as? UIViewController
            }
            if iOSDeviceScreenSize.height == 667 {
                var iPhone47InchStoryBoard = UIStoryboard(name: "Storyboard_iPhone_6", bundle: nil)
                initialViewController = iPhone47InchStoryBoard.instantiateInitialViewController() as? UIViewController
            }
            if iOSDeviceScreenSize.height == 736 {
                var iPhone55InchStoryBoard = UIStoryboard(name: "Storyboard_iPhone_6_plus", bundle: nil)
                initialViewController = iPhone55InchStoryBoard.instantiateInitialViewController() as? UIViewController
            }
            
            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        */
        
        // Use this the first time, against the new server.
//        var appDomain = NSBundle.mainBundle().bundleIdentifier
//        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain!)

        // Check if the user is stored locally, if so set the currentUser
        var defaults = NSUserDefaults.standardUserDefaults()
        var myEncodedObject = defaults.objectForKey("current user") as! NSData?
        if myEncodedObject != nil {
            currentUser = NSKeyedUnarchiver.unarchiveObjectWithData(myEncodedObject!) as? User
            
            Flurry.setGender(currentUser?.gender)
            if let age = currentUser?.age {
                Flurry.setAge( Int32(age) )
            }
        }
        
        // Set the tab bar objects text and image tint colors. -> important - changes everywhere.
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: CGFloat(0/255.0), green: CGFloat(0/255.0), blue: CGFloat(0/255.0), alpha: CGFloat(0.5))], forState:.Selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name:"Arial Rounded MT Bold", size:11.0)!], forState:.Normal)
        UITabBar.appearance().tintColor = UIColor(red: CGFloat(77/255.0), green: CGFloat(175/255.0), blue: CGFloat(101/255.0), alpha: CGFloat(1.0))
        
        return true
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        var wasHandled:Bool = FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
        return wasHandled
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // Save the use locally.
        if currentUser != nil {
            var defaults = NSUserDefaults.standardUserDefaults()
            var myEncodedObject = NSKeyedArchiver.archivedDataWithRootObject(currentUser!)
        
            defaults.setObject(myEncodedObject, forKey: "current user")
            defaults.synchronize()
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}


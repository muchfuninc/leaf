//
//  Settings.swift
//  Leaf
//
//  Created by Nichlas Lendal on 30/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public let config : Config = Config()

public class Config : NSObject {
    
    public let
    //Hvis sat til true, koeres der mod live api'er. (Facebook & QuestionsService)
    runOnline : Bool = true,
    
    //Base URL
    serverUrl = "http://128.199.62.112:39274",
    api_url = "http://128.199.62.112:39274/api/",

    //Logging
    logLevel : LogLevel = LogLevel.TRACE,
    logDateFormat : String = "yyyy-MM-dd HH:mm:ss.SSS"
    
}

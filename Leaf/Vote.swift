//
//  Vote.swift
//  Leaf
//
//  Created by Martin Elvar on 31/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public class Vote {
    var
    positive: Bool,
    neutral: Bool,
    negative: Bool,
    questionId: Int
    
    init(positive: Bool, neutral: Bool, negative: Bool, questionId: Int) {
        self.positive = positive
        self.neutral = neutral
        self.negative = negative
        self.questionId = questionId
    }
    
    func asParams() -> [String: AnyObject] {
        let params = [
            "vote" : [
                "positive"    : self.positive,
                "neutral"     : self.neutral,
                "negative"    : self.negative,
                "question_id" : self.questionId,
            ]
        ]
        
        return params
    }
}
//
//  Users.swift
//  Leaf
//
//  Created by Martin Elvar on 29/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

struct Users: JSONJoy {
    var users: Array<User> = []

    init(_ decoder: JSONDecoder) {
        if let decoderArray = decoder["users"].array {
            for userDecoder in decoderArray {
                users.append(User(userDecoder))
            }
        }
    }
}
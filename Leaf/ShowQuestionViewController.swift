import UIKit
import AVFoundation

class ShowQuestionViewController: UIViewController {
    
    internal var question: Question!
    
    @IBOutlet var questionBody: UILabel!
    @IBOutlet var questionImage: UIImageView!
    @IBOutlet var positiveCounter: UILabel!
    @IBOutlet var neutralCounter: UILabel!
    @IBOutlet var negativeCounter: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userImage.image = loadExternalImage(question.authorImage)
        userName.text = question.author
        questionImage.image = loadExternalImage(question.image)
        questionBody.text = question.body
        positiveCounter.text = String(question.positive)
        neutralCounter.text = String(question.neutral)
        negativeCounter.text = String(question.negative)
        
        userImage?.clipsToBounds = true
        var profileImageLayer = userImage!.layer
        profileImageLayer.cornerRadius = userImage!.frame.size.width / 2
        profileImageLayer.borderWidth = 2.0
        profileImageLayer.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.2).CGColor
    }
    
    func setQuestion(question: Question) {
        self.question = question
    }
    
    @IBAction func closeView(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  User.swift
//  Leaf
//
//  Created by Nichlas Lendal on 26/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public class User : NSObject, NSCoding {

    var
    id                  : Int?,
    name                : String,
    email               : String,
    createdAt           : String?,
    locale              : String,
    gender              : String,
    age                 : Int?,
    facebookId          : String,
    facebookAccessToken : String?,
    timezone            : Int,
    image               : String?,
    apiKey              : String?

    public init(_ id: Int, _ name: String, _ email: String, createdAt: String, locale: String, gender: String, age: Int, facebookId: String, timezone: Int, image: String) {
        self.id = id
        self.name = name
        self.email = email
        self.createdAt = createdAt
        self.locale = locale
        self.gender = gender
        self.age = age
        self.facebookId = facebookId
        self.timezone = timezone
        self.image = image
    }

    init(_ decoder: JSONDecoder) {
        self.id = decoder["user"]["id"].integer!
        self.name = decoder["user"]["name"].string!
        self.email = decoder["user"]["email"].string!
        self.createdAt = decoder["user"]["created_at"].string!
        self.locale = decoder["user"]["locale"].string!
        self.gender = decoder["user"]["gender"].string!
        //self.age = decoder["user"]["age"].integer!
        self.facebookId = decoder["user"]["facebook_id"].string!
        self.timezone = decoder["user"]["timezone"].integer!
        self.image = decoder["user"]["image"].string!
        self.apiKey = decoder["user"]["api_key"].string!
    }
    
    init(fbGraph: FBGraphUser) {
        self.facebookId = fbGraph.objectID
        self.name = fbGraph.name
        self.email = fbGraph.objectForKey("email") as! String
        self.locale = fbGraph.objectForKey("locale") as! String
        self.gender = fbGraph.objectForKey("gender") as! String
        
        //if (fbGraph.objectForKey("age_range") != nil){
            //self.age = fbGraph.objectForKey("age_range") as! Int
        //}
        
        self.timezone = fbGraph.objectForKey("timezone") as! Int
        self.facebookAccessToken = FBSession.activeSession().accessTokenData.accessToken
    }
    
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObjectForKey("id") as? Int
        self.name = aDecoder.decodeObjectForKey("name") as! String!
        self.email = aDecoder.decodeObjectForKey("email") as! String!
        self.createdAt = aDecoder.decodeObjectForKey("createdAt") as! String!
        self.locale = aDecoder.decodeObjectForKey("locale") as! String!
        self.gender = aDecoder.decodeObjectForKey("gender") as! String!
        //self.age = aDecoder.decodeObjectForKey("age") as! Int!
        self.facebookId = aDecoder.decodeObjectForKey("facebookId") as! String!
        self.timezone = aDecoder.decodeObjectForKey("id") as! Int!
        self.image = aDecoder.decodeObjectForKey("image") as! String!
        self.apiKey = aDecoder.decodeObjectForKey("apiKey") as! String!
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.id, forKey: "id")
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeObject(self.email, forKey: "email")
        aCoder.encodeObject(self.createdAt, forKey: "createdAt")
        aCoder.encodeObject(self.locale, forKey: "locale")
        aCoder.encodeObject(self.gender, forKey: "gender")
        //aCoder.encodeObject(self.age, forKey: "age")
        aCoder.encodeObject(self.facebookId, forKey: "facebookId")
        aCoder.encodeObject(self.timezone, forKey: "timezone")
        aCoder.encodeObject(self.image, forKey: "image")
        aCoder.encodeObject(self.apiKey, forKey: "apiKey")
    }
    
    func asParams() -> [String: AnyObject] {
        let params = [
            "user" : [
                "facebook_id"           : self.facebookId,
                "facebook_access_token" : self.facebookAccessToken!,
                "name"                  : self.name,
                "email"                 : self.email,
                "locale"                : self.locale,
                "gender"                : self.gender,
                "timezone"              : self.timezone
            //TODO: Add Age..
            ]
        ]

        return params
    }
}

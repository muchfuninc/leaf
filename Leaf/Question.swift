//
//  Question.swift
//  Leaf
//
//  Created by Nichlas Lendal on 26/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public struct Question {
    
    let
    id : Int,
    author: String,
    authorImage: String,
    image: String,
    thumb: String,
    createdAt: String,
    body: String,
    positive: Int,
    neutral: Int,
    negative: Int
    
    init(id: Int, author: String, authorImage: String, image: String, thumb: String, createdAt: String, body: String, positive: Int, neutral: Int, negative: Int) {
        self.id = id
        self.author = author
        self.authorImage = authorImage
        self.image = image
        self.thumb = thumb
        self.createdAt = createdAt
        self.body = body
        self.positive = positive
        self.neutral = neutral
        self.negative = negative
    }

    init(_ decoder: JSONDecoder) {
        self.id = decoder["id"].integer!
        self.body = decoder["body"].string!
        self.thumb = config.serverUrl + decoder["thumb"].string!
        self.image = config.serverUrl + decoder["image"].string!
        self.positive = decoder["positive"].integer!
        self.neutral = decoder["neutral"].integer!
        self.negative = decoder["negative"].integer!
        self.author = decoder["author"].string!
        self.authorImage = decoder["author_image"].string!
        self.createdAt = decoder["created_at"].string!
    }
}

import UIKit

class CarouselViewController: UIViewController {

    @IBOutlet var yeiBtn: UIButton?
    @IBOutlet var neiBtn: UIButton?
    @IBOutlet var nextBtn: UIButton?
    @IBOutlet var questionImageView: QuestionImageView?
    @IBOutlet var profileImageView: UIImageView?
    @IBOutlet var questionText: UILabel?
    @IBOutlet var authorTitle: UILabel?
    
    var questionQueue: QuestionQueue! = QuestionQueue()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.yeiBtn?.alpha = 0
        self.neiBtn?.alpha = 0
        self.nextBtn?.alpha = 0
        self.questionImageView?.alpha = 0
        self.profileImageView?.alpha = 0
        self.questionText?.alpha = 0
        self.authorTitle?.alpha = 0
        
        UIView.animateWithDuration(0.6, animations: {
            self.yeiBtn?.alpha = 1
            self.neiBtn?.alpha = 1
            self.nextBtn?.alpha = 1
            self.questionImageView?.alpha = 1
            self.profileImageView?.alpha = 1
            self.questionText?.alpha = 1
            self.authorTitle?.alpha = 1
        })
        // TODO: Check if the user is authenticated, by both facebook and API.
        // If not slide in authenticate view.
        if currentUser == nil && config.runOnline {
            let authController = self.storyboard!.instantiateViewControllerWithIdentifier("AuthenticateView") as! AuthViewController

            UIView.animateWithDuration(0.001, animations: {
                }, completion: { finished in
                    self.navigationController?.pushViewController(authController, animated: false)
                }
            )
            return
        }

        // Reset navigation bar visibility.
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        setProfilePictureTheming()

        // Set the queue for the questionImageView, when ready.
        questionQueue.onReady { () -> () in
            // Perform update on main queue to make sure ui is propper loaded.
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.questionImageView!.setQuestionQueue(self.questionQueue)
            }
            self.updateQuestion()
        }

        // Update ui each time next question is called.
        questionQueue.onNext { () -> () in
            self.updateQuestion()
        }
        
        questionQueue.onLoad { () -> () in
            // Show a loader.
            Leaf.logDebug("Loading a new batch")
        }
        
        questionQueue.onEmpty { () -> () in
            // Inform the user.
            Leaf.logDebug("Queue is empty")
        }

        // Set closures for slide event in the question view.
        questionImageView?.onNegative = { self.pushVoteAndNext("negative") }
        questionImageView?.onPositive = { self.pushVoteAndNext("positive") }
        
        // Start the queue.
        self.questionQueue.initializeQueue()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        // Make question image view, use the same questionQueue.
        questionImageView?.questionQueue = questionQueue
    }

    func updateQuestion() {
        // Update author information.
        var question = questionQueue.getCurrentQuestion()
       
        NSOperationQueue.mainQueue().addOperationWithBlock {
            var err: NSError?

            self.authorTitle!.text = question.author
    
            var profileImageUrl = NSURL(string: question.authorImage)
            var profileImageData = NSData(contentsOfURL: profileImageUrl!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
            var profileImage = UIImage(data: profileImageData!)

            self.profileImageView?.setValue(profileImage, forKey: "image")
            self.questionText!.text = question.body
        }
    }

    func setProfilePictureTheming() {
        profileImageView?.clipsToBounds = true
        
        var profileImageLayer = profileImageView!.layer
        profileImageLayer.cornerRadius = profileImageView!.frame.size.width / 2
        profileImageLayer.borderWidth = 3.0
        profileImageLayer.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.2).CGColor
    }
    
    func setBtnTheming() {
        var yayBtnLayer = yeiBtn?.layer
        yayBtnLayer?.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.9).CGColor
        yayBtnLayer?.borderWidth = 3.0
        yayBtnLayer?.cornerRadius = 15.0
        
        var nopeBtnLayer = neiBtn?.layer
        nopeBtnLayer?.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.9).CGColor
        nopeBtnLayer?.borderWidth = 3.0
        nopeBtnLayer?.cornerRadius = 15.0
        
        var skipBtnLayer = nextBtn?.layer
        skipBtnLayer?.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.9).CGColor
        skipBtnLayer?.borderWidth = 3.0
        skipBtnLayer?.cornerRadius = 12.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Events.
    @IBAction func didClickPositive(sender: UIButton) {
        pushVoteAndNext("positive")
    }
    
    @IBAction func didClickNeutral(sender: UIButton) {
        pushVoteAndNext("neutral")
    }
    
    @IBAction func didClickNegative(sender: UIButton) {
        pushVoteAndNext("negative")
    }
    
    private func pushVoteAndNext(voteType: String) {
        var vote = Vote(positive: false, neutral: false, negative: false, questionId: questionQueue.getCurrentQuestion().id)
        
        switch voteType {
        case "positive":
            vote.positive = true
        case "negative":
            vote.negative = true
        default:
            vote.neutral = true
        }
        
        voteService.createVote(vote) {
            statusCode, error in
                Leaf.logError("Failed to pushVote - statusCode: \(statusCode)",
                    error: error)
        }
        
        Leaf.logEvent(LeafEvents.CAROUSEL_QUESTION_VOTED, withParameters: ["Vote": voteType])

        self.questionQueue.next()
    }
}

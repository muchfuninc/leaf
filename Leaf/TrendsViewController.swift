import UIKit
import AVFoundation

class TrendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate {

    @IBOutlet var listingTable: UITableView!

    // Tab bar, and belonging elements.
    @IBOutlet var ListingTabBar: UITabBar!
    @IBOutlet var positiveButton: UITabBarItem!
    @IBOutlet var neutralButton: UITabBarItem!
    @IBOutlet var negativeButton: UITabBarItem!

    var positiveQuestions: [Question] = []
    var neutralQuestions: [Question] = []
    var negativeQuestions: [Question] = []

    var positiveQuestionsImages: [UIImage] = []
    var neutralQuestionsImages: [UIImage] = []
    var negativeQuestionsImages: [UIImage] = []

    var currentListing: [Question] = []
    var currentImageListing: [UIImage] = []
    var chosenQuestion: Question?
    
    var currentListFilter : String = "positive"

    override func viewDidLoad() {
        super.viewDidLoad()
        Flurry.logAllPageViewsForTarget(ListingTabBar)

        // Set yayed as initial selected item.
        ListingTabBar.selectedItem = positiveButton

        listingTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Start by fetching the positive questions.
        questionsService.getQuestions(25, filter: "positive",
            success: { questions in
                self.positiveQuestions = questions!
                self.positiveQuestionsImages = self.loadThumbs(questions!)

                self.currentListing = self.positiveQuestions
                self.currentImageListing = self.positiveQuestionsImages

                dispatch_async(dispatch_get_main_queue()) { queue in
                    self.listingTable.reloadData()
                }
            },
            error: { statusCode, error in
                Leaf.logError("Failed to load questions - statusCode: \(statusCode)",
                    error: error)
            }
        )
    }

    // Synchronously load all thumbnails.
    func loadThumbs(list: [Question]) -> [UIImage] {
        var images: [UIImage] = []

        for question in list {
            var err: NSError?
            let imageURL = NSURL(string: question.thumb)
            let imageData = NSData(contentsOfURL: imageURL!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
            images.append(UIImage(data: imageData!)!)
        }

        return images
    }

    private func changeList(list: String) {
        currentListFilter = list
        var issuedList: [Question]!
        var issuedImageList: [UIImage]!

        switch list {
        case "positive":
            issuedList = positiveQuestions
            issuedImageList = positiveQuestionsImages
        case "neutral":
            issuedList = neutralQuestions
            issuedImageList = neutralQuestionsImages
        case "negative":
            issuedList = negativeQuestions
            issuedImageList = negativeQuestionsImages
        default:
            issuedList = positiveQuestions
            issuedImageList = positiveQuestionsImages
        }

        // If already loaded once, use that cached version.
        if issuedList.count > 0 {
            currentListing = issuedList
            currentImageListing = issuedImageList
            dispatch_async(dispatch_get_main_queue()) { queue in
                self.listingTable.reloadData()
            }

            return
        }

        questionsService.getQuestions(25, filter: list,
            success: { questions in
                // Blame Swift's lack of pointer support!
                // Set questions and images.
                switch list {
                case "positive":
                    self.positiveQuestionsImages = self.loadThumbs(questions!)
                    self.currentImageListing = self.positiveQuestionsImages

                    self.positiveQuestions = questions!
                case "neutral":
                    self.neutralQuestionsImages = self.loadThumbs(questions!)
                    self.currentImageListing = self.neutralQuestionsImages

                    self.neutralQuestions = questions!
                case "negative":
                    self.negativeQuestionsImages = self.loadThumbs(questions!)
                    self.currentImageListing = self.negativeQuestionsImages

                    self.negativeQuestions = questions!
                default:
                    self.positiveQuestionsImages = self.loadThumbs(questions!)
                    self.currentImageListing = self.positiveQuestionsImages

                    self.positiveQuestions = questions!
                }

                // Set the current listing.
                self.currentListing = questions!
            
                dispatch_async(dispatch_get_main_queue()) { queue in
                    self.listingTable.reloadData()
                }
            },
            error: { statusCode, error in
                Leaf.logError("Failed to load questions - statusCode: \(statusCode)",
                    error: error)
            }
        )
    }

    /**
    * Table delegation methods
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentListing.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: QuestionsTableViewCell = self.listingTable.dequeueReusableCellWithIdentifier("Cell") as! QuestionsTableViewCell
        cell.questionText.text = self.currentListing[indexPath.row].body
        cell.questionNumber.text = String(indexPath.row + 1)
        cell.Yays.text = String(self.currentListing[indexPath.row].positive)
        cell.Nopes.text = String(self.currentListing[indexPath.row].negative)
        cell.Skips.text = String(self.currentListing[indexPath.row].neutral)
        cell.questionImage.image = self.currentImageListing[indexPath.row]
        cell.questionImage.clipsToBounds = true
        var profileImageLayer = cell.questionImage.layer
        profileImageLayer.cornerRadius = 10.0
        profileImageLayer.borderWidth = 3.0
        profileImageLayer.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.2).CGColor

        return cell
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Leaf.logEvent(LeafEvents.TRENDS_QUESTION_SHOWED, withParameters: ["atIndex": indexPath.row, "inList": currentListFilter])
        chosenQuestion = currentListing[indexPath.row]
        performSegueWithIdentifier("trendsToShowQuestion", sender: self)
    }

    /**
    * Tab bar delegation methods
    */
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem!) {
        switch item {
        case positiveButton:
            changeList("positive")
        case neutralButton:
            changeList("neutral")
        case negativeButton:
            changeList("negative")
        default:
            changeList("positive")
        }
    }
    
    // Pass along the chosen question, to prevent a extra api call.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "trendsToShowQuestion" {
            var controller = segue.destinationViewController as! ShowQuestionViewController
            controller.setQuestion(chosenQuestion!)
        }
    }

    @IBAction func didClickBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

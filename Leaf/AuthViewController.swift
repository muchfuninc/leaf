import Foundation
import UIKit

class AuthViewController: UIViewController, FBLoginViewDelegate  {

    @IBOutlet var fbLoginView : FBLoginView!
    var lockUser: FBGraphUser?
    @IBOutlet var splashScreen : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        fbLoginView.delegate = self
        self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
    }

    // Facebook Delegate Methods
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        Leaf.logEvent(LeafEvents.AUTH_LOGGEDIN)
    }

    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
        // Prevent a facebook bug from executing this twice.
        if (self.lockUser != nil && self.runAuth(user)) {
            return
        }
        self.lockUser = user
        var mappedUser = User(fbGraph: user)
        
        userService.authenticate(mappedUser,
            success: {
                // Can this be put on queue of some sort?
                delay(0.5) {
                    let CarouselController = self.storyboard!.instantiateViewControllerWithIdentifier("CarouselView") as! CarouselViewController
                    UIView.animateWithDuration(0.5, animations: {
                        self.splashScreen.alpha = 0
                        self.fbLoginView.alpha = 0
                        }, completion: { finished in
                            self.navigationController?.pushViewController(CarouselController, animated: false)
                        }
                    )
                }
            },
            error: { statusCode, error in
                Leaf.logError("Failed to authenticate user - statusCode: \(statusCode)",
                    error: error)
            })
    }

    private func runAuth(user: FBGraphUser) -> (Bool) {
        return self.lockUser!.objectID == user.objectID
    }

    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        Leaf.logEvent(LeafEvents.AUTH_LOGGEDOUT)
    }

    func loginView(loginView : FBLoginView!, handleError:NSError) {
        Leaf.logError("Got an error from FBLoginView",
            error: handleError)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

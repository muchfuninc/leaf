//
//  UserService.swift
//  Leaf
//
//  Created by Nichlas Lendal on 27/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public var currentUser : User?

public var userService : UserService = UserService(FinkAPI())

public class UserService: NSObject {

    let finkAPI : FinkAPI
    
    init(_ finkAPI: FinkAPI){
        self.finkAPI = finkAPI
    }
    
    func getUser(success: (user: User) -> (), error: (statusCode: Int, error: NSError?) -> ()) {
        /*RequestBuilder
            .GET(user1)
            .onSuccess({ data in success(user:  })
            .onError(error, queue: NSOperationQueue.mainQueue())
            .execute()*/
    }
    
    func authenticate(user: User, success: ()->(), error: (statusCode: Int, error: NSError?) -> ()) {
        RequestBuilder
            .POST(finkAPI.users())
            .withParams(user.asParams())
            .onSuccess( { data in
                // Save user data to a global currentUser variable.
                currentUser = User(JSONDecoder(data))
                success()
            })
            .execute()
    }
    
    func getUsers(success: (users: [User]?) -> (), error: (statusCode: Int, error: NSError?) -> ()) {
        RequestBuilder
            .GET(finkAPI.users())
            .onSuccess({ data in
                success(users: Users(JSONDecoder(data)).users )
            })
            .onError(error, queue: NSOperationQueue.mainQueue())
            .execute()
    }
}

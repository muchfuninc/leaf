//
//  DraggableView.swift
//  Leaf
//
//  Created by Nichlas Lendal on 05/11/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

class DraggableView : UIView {
    
    var startingOffset : CGPoint? //saa man dragge fra andre steder end toppen af viewet.
    
    var topFollower : UIView?
    var bottomFollower : UIView?
    
    let totalHeight = UIScreen.mainScreen().bounds.size.height
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        var aTouch = touches.first as! UITouch
        startingOffset = aTouch.locationInView(self)
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        var aTouch = touches.first as! UITouch
        var location : CGPoint = aTouch.locationInView(self.superview)
        
        UIView.beginAnimations(nil, context:nil)

        var newDraggableRect = CGRectMake(0, location.y - startingOffset!.y, self.frame.size.width, self.frame.size.height)
        
        var newTopRect = CGRectMake(0, 0, self.frame.size.width, newDraggableRect.origin.y)
        
        var newBottomRect = CGRectMake(0, (newDraggableRect.origin.y + newDraggableRect.height), self.frame.size.width, (totalHeight -  newDraggableRect.origin.y))
        
        self.frame = newDraggableRect
        topFollower?.frame = newTopRect
        bottomFollower?.frame = newBottomRect
        UIView.commitAnimations()
    }
    
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        //Hvis brugeren har flyttet draggable area ud af skaermen. Flyt den tilbage.
        var aTouch = touches.first as! UITouch
        var location : CGPoint = aTouch.locationInView(self.superview)
        
        //for hoejt.
        if self.frame.origin.y < 0 {
            
            UIView.beginAnimations(nil, context:nil)
            var newDraggableRect = CGRectMake(0, 20, self.frame.size.width, self.frame.size.height) //fix 20 her.
            var newBottomRect = CGRectMake(0, (newDraggableRect.origin.y + newDraggableRect.height), self.frame.size.width, (totalHeight -  newDraggableRect.origin.y))
            self.frame = newDraggableRect
            self.bottomFollower?.frame = newBottomRect
            UIView.commitAnimations()
        
        }
        //for lavt.
        else if self.frame.origin.y > (totalHeight - self.frame.height) {
            
            UIView.beginAnimations(nil, context:nil)
            var newDraggableRect = CGRectMake(0, totalHeight - self.frame.height, self.frame.size.width, self.frame.size.height) //fix 20 her.
            var newTopRect = CGRectMake(0, 0, self.frame.size.width, newDraggableRect.origin.y)
            self.frame = newDraggableRect
            self.topFollower?.frame = newTopRect
            UIView.commitAnimations()
            
        }
        
        
    }
}

//
//  QuestionsStubService.swift
//  Leaf
//
//  Created by Nichlas Lendal on 26/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public var questionsStubService : QuestionsStubService = QuestionsStubService()

public class QuestionsStubService: NSObject {

    private let dummyQuestions = [
        Question(
            id: 1,
            author: "David Hammer",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Er david awesome?",
            positive: 2,
            neutral: 5,
            negative: 20
        ),
        Question(
            id: 2,
            author: "Martin Elvar",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Er Martin awesome?",
            positive: 232,
            neutral: 5,
            negative: 20
        ),
        Question(
            id: 3,
            author: "Nichlas Lendal",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Er Nichlas awesome?",
            positive: 21,
            neutral: 52,
            negative: 220
        ),
        Question(
            id: 4,
            author: "Torben Nielce",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Ligner jeg en dude der har sniffet coke?",
            positive: 290342,
            neutral: 5,
            negative: 20
        ),
        Question(
            id: 5,
            author: "Oste Far",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Lugter jeg af ost?",
            positive: 2,
            neutral: 5000,
            negative: 20
        ),
        Question(
            id: 6,
            author: "Fisse far",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Oste i poolen?",
            positive: 2,
            neutral: 5,
            negative: 20
        ),
        Question(
            id: 7,
            author: "Torben Hansen",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Er david awesome?",
            positive: 22,
            neutral: 52,
            negative: 20
        ),
        Question(
            id: 8,
            author: "Terkel Andersen",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Ligner jeg en regnorm?",
            positive: 20000,
            neutral: 5,
            negative: 20
        ),
        Question(
            id: 9,
            author: "Ahmed Jorndal",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Ligner jeg muhammed?",
            positive: 20000,
            neutral: 522,
            negative: 2220
        ),
        Question(
            id: 10,
            author: "Fest Andersen",
            authorImage : "https://graph.facebook.com/23232/picture?type=large",
            image: "http://i.imgur.com/JGI8ZGS.jpg",
            thumb: "http://i.imgur.com/JGI8ZGS.jpg",
            createdAt: "2014-10-28T22:33:49.258Z",
            body: "Fester jeg for vildt?",
            positive: 200,
            neutral: 90,
            negative: 20
        ),
    ]
    
    func getQuestions() -> Array<Question> {
        return dummyQuestions
    }

}

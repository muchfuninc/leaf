//
//  Questions.swift
//  Leaf
//
//  Created by Martin Elvar on 29/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

struct Questions: JSONJoy {
    var questions: Array<Question> = []

    init(_ decoder: JSONDecoder) {
        if let decoderArray = decoder["questions"].array {
            for questionDecoders in decoderArray {
                questions.append(Question(questionDecoders))
            }
        }
    }
}
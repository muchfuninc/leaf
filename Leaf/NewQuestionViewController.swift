import UIKit
import AVFoundation

class NewQuestionViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var navigationBar: UINavigationItem?
    
    @IBOutlet weak var cameraView: UIView!
    
    @IBOutlet weak var changeCameraButton: UIButton!
    @IBOutlet weak var changeFlashModeButton: UIButton!
    
    let captureSession = AVCaptureSession()
    var previewLayer  : AVCaptureVideoPreviewLayer?
    var captureDevice : AVCaptureDevice?
    var currentDeviceInput : AVCaptureDeviceInput?
    var currentDeviceOutput : AVCaptureStillImageOutput?
    var currentDevicePosition : AVCaptureDevicePosition?
    
    var currentFlashMode : AVCaptureFlashMode = AVCaptureFlashMode.Off //saet default som off.
    let screenWidth = UIScreen.mainScreen().bounds.size.width
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        setupBackCamera()
    }
    
    @IBAction func captureImage(sender: UIButton) {
        if let connection = findVideoConnection(currentDeviceOutput!) {
            currentDeviceOutput!.captureStillImageAsynchronouslyFromConnection(connection,
                completionHandler: { buffer, error in
                    var data : NSData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                    var image = UIImage(data: data)!
                    
                    //Mirror the image..
                    if (self.currentDevicePosition == AVCaptureDevicePosition.Front){
                        var flippedImage = UIImage(CGImage: image.CGImage, scale: image.scale, orientation: UIImageOrientation.LeftMirrored)!
                        image = flippedImage
                    }
                    
                    var scaled =  ImageUtils.cropImageByView(image, view: self.cameraView)
                    var sized = ImageUtils.resizeImage(scaled, targetSize: CGSize(width: 800, height: 800))
                    self.onCapturedOrSelectedImage(sized)
            })
        }
    }
    
    @IBAction func changeCamera(sender: UIButton) {
        if currentDevicePosition == AVCaptureDevicePosition.Back {
            setupFrontCamera()
            changeCameraButton.setTitle("Front", forState: UIControlState.Normal)
        } else {
            setupBackCamera()
            changeCameraButton.setTitle("Back", forState: UIControlState.Normal)
        }
    }
    
    @IBAction func changeFlashMode(sender: UIButton) {
        if let device = captureDevice { //Er der et device?
            if device.hasFlash { //Har den flash?
                
                //calculate next flash mode.
                var nextFlashMode = self.calculateNextFlashMode(currentFlashMode)
                if (device.isFlashModeSupported(nextFlashMode!) == false){
                    nextFlashMode = AVCaptureFlashMode.Off //slaa fra.
                    currentFlashMode = nextFlashMode!
                    return
                }
                
                //flash mode er supported.
                if(device.lockForConfiguration(nil)) {
                    device.flashMode = nextFlashMode!
                    currentFlashMode = nextFlashMode!
                    onUpdatingFlashMode() //opdaterer knappens text, og senere den billede.
                    device.unlockForConfiguration()
                }
            } else {
                Leaf.logDebug("Flash knappen vises, men device har ikke flash!!!!!")
            }
        }
    }
    
    private func calculateNextFlashMode(current: AVCaptureFlashMode) -> AVCaptureFlashMode? {
        if current == AVCaptureFlashMode.Off {
            return AVCaptureFlashMode.On
        }
        else if current == AVCaptureFlashMode.On {
            return AVCaptureFlashMode.Auto
        }
        else if current == AVCaptureFlashMode.Auto {
            return AVCaptureFlashMode.Off
        }
        return nil
    }
    
    private func onUpdatingFlashMode() {
        if currentFlashMode == AVCaptureFlashMode.Off {
            changeFlashModeButton.setTitle("OFF", forState: UIControlState.Normal)
        }
        else if currentFlashMode == AVCaptureFlashMode.On {
            changeFlashModeButton.setTitle("ON", forState: UIControlState.Normal)
        }
        else if currentFlashMode == AVCaptureFlashMode.Auto {
            changeFlashModeButton.setTitle("AUTO", forState: UIControlState.Normal)
        }
    }
    
    func findVideoConnection(output: AVCaptureStillImageOutput) -> AVCaptureConnection? {
        var videoConnection : AVCaptureConnection?
        for connection in output.connections {
            for port in connection.inputPorts! {
                if port.mediaType == AVMediaTypeVideo {
                    videoConnection = connection as? AVCaptureConnection
                    break
                }
            }
            if videoConnection != nil { break }
        }
        return videoConnection
    }
    
    @IBAction func onCapturedOrSelectedImage(image: UIImage) {
        Leaf.logEvent(LeafEvents.QUESTION_PHOTO_CAPTURED, withParameters: ["withCamera": changeCameraButton.titleForState(UIControlState.Normal)!, "flashMode": changeFlashModeButton.titleForState(UIControlState.Normal)!])
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("CreateQuestionView") as! CreateQuestionViewController
        controller.injectedImage = image

        // Define where to go, after the question has been pushed to the server.
        controller.redirectCallback = {
            NSOperationQueue.mainQueue().addOperationWithBlock {
                let profileController = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileView") as! ProfileViewController
                self.navigationController?.pushViewController(profileController, animated: false)
            }
        }

        self.presentViewController(controller, animated: false, completion: nil)
    }
    
    private func setupBackCamera(){
        if captureSession.running {
            self.stopAndClearSession()
        }
        
        configureBackCameraDevice()
        
        captureDevice = findCaptureDeviceByPosition(AVCaptureDevicePosition.Back)
        if  captureDevice != nil {
            currentDevicePosition = AVCaptureDevicePosition.Back
            var err : NSError? = nil
            currentDeviceInput = AVCaptureDeviceInput(device: captureDevice, error: &err)
            if err != nil {
                Leaf.logError("Failed to set up AVCaptureDeviceInput",
                    error: err)
            }
            
            currentDeviceOutput = AVCaptureStillImageOutput()
            currentDeviceOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG, AVVideoQualityKey: 0.5]
            captureSession.addOutput(currentDeviceOutput)
            
            beginSession()
        }
    }
    
    private func setupFrontCamera(){
        if captureSession.running {
            self.stopAndClearSession()
        }
        
        captureDevice = findCaptureDeviceByPosition(AVCaptureDevicePosition.Front)
        if  captureDevice != nil {
            currentDevicePosition = AVCaptureDevicePosition.Front
            var err : NSError? = nil
            currentDeviceInput = AVCaptureDeviceInput(device: captureDevice, error: &err)
            if err != nil {
                Leaf.logError("Failed to set up AVCaptureDeviceInput",
                    error: err)
            }
            
            currentDeviceOutput = AVCaptureStillImageOutput()
            currentDeviceOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession.addOutput(currentDeviceOutput)
            
            beginSession()
        }
    }
    
    private func beginSession() {
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        captureSession.addInput(currentDeviceInput!)
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.cameraView?.layer.addSublayer(previewLayer)
        if let var view = self.cameraView {
            previewLayer?.frame = view.layer.frame
        }
        captureSession.startRunning()
    }
    
    private func stopAndClearSession(){
        captureSession.stopRunning()
        captureSession.removeInput(currentDeviceInput)
        captureSession.removeOutput(currentDeviceOutput)
    }
    
    private func configureBackCameraDevice() {
        if let device = captureDevice {
            if (device.lockForConfiguration(nil)){
                
                if (device.isFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus)){
                    device.focusMode = AVCaptureFocusMode.ContinuousAutoFocus
                }
                
                if (device.isExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure)){
                    device.exposureMode = AVCaptureExposureMode.ContinuousAutoExposure
                }
    
                device.unlockForConfiguration()
            }
        }
    }
    
    //Listens for touches on screen, and maps to a CGPoint which tells us the point of interrest.
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        var anyTouch = touches.first as! UITouch
        var pointTouched = anyTouch.locationInView(self.cameraView)
        self.setPointOfInterest(pointTouched)
    }
    
    //Focus funktionality.
    func setPointOfInterest(pointOfInterest : CGPoint) {
        if let device = captureDevice {
            if(device.lockForConfiguration(nil)) {
                
                if (device.isFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus)){
                    device.focusPointOfInterest = pointOfInterest
                    device.focusMode = AVCaptureFocusMode.ContinuousAutoFocus
                }
                
                if (device.isExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure)){
                    device.exposurePointOfInterest = pointOfInterest
                    device.exposureMode = AVCaptureExposureMode.ContinuousAutoExposure
                }
            
                device.unlockForConfiguration()
            }
        }
    }

    private func findCaptureDeviceByPosition(postion: AVCaptureDevicePosition) -> AVCaptureDevice? {
        for device in AVCaptureDevice.devices() {
            if (device.hasMediaType(AVMediaTypeVideo)) {
                if device.position == postion {
                    return device as? AVCaptureDevice
                }
            }
        }
        return nil
    }
    
    /*
    @IBAction func chooseImageFromPhotoLibrary() {
        //goto library controller...
        let libraryViewCtrl = self.storyboard!.instantiateViewControllerWithIdentifier("LibraryView") as! LibraryViewController
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .PhotoLibrary
        presentViewController(picker, animated: true, completion: {})
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        let libraryViewCtrl = self.storyboard!.instantiateViewControllerWithIdentifier("LibraryView") as! LibraryViewController
        
        libraryViewCtrl.pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        dismissViewControllerAnimated(true, completion: nil)
        presentViewController(libraryViewCtrl, animated: false, completion: {})
    }
    */
    
    func addBackButton() {
        var carouselIcon = UIImage(named: "CarouselNavigationIcon")
        var myBackButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        myBackButton.addTarget(self, action: "popToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
        myBackButton.setTitle("", forState: UIControlState.Normal)
        myBackButton.titleLabel?.font = UIFont(name: "MarkerFelt-Thin", size: 30)
        myBackButton.setBackgroundImage(carouselIcon, forState: UIControlState.Normal)
        myBackButton.tintColor = UIColor(red: CGFloat(50/255.0), green: CGFloat(50/255.0), blue: CGFloat(50/255.0), alpha: CGFloat(1.0))
        myBackButton.sizeToFit()
        var myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
    }

    @IBAction func goToCarousel() {
        let carouselController = self.storyboard!.instantiateViewControllerWithIdentifier("CarouselView") as! CarouselViewController
        self.navigationController?.pushViewController(carouselController, animated: true)
    }

    func popToRoot(sender:UIBarButtonItem){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}

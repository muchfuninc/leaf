import UIKit
import AVFoundation

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var profileImageView: UIImageView?
    @IBOutlet var historyTable: UITableView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var navigationBar: UINavigationItem?
    
    var questions: [Question] = []
    var questionImages: [UIImage] = []
    var chosenQuestion: Question?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Load & set profile image.
        var err: NSError?
        self.profileImageView?.clipsToBounds = true
        var profileImageUrl = NSURL(string: currentUser!.image!)
        var profileImageData = NSData(contentsOfURL: profileImageUrl!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
        var profileImage = UIImage(data: profileImageData!)!
        self.profileImageView?.setValue(profileImage, forKey: "image")
        
        // Theme profile image view.
        self.profileImageView?.clipsToBounds = true
        var profileImageLayer = profileImageView!.layer
        profileImageLayer.cornerRadius = profileImageView!.frame.size.width / 2
        profileImageLayer.borderWidth = 3.0
        profileImageLayer.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.2).CGColor

        // Set profile information.
        nameLabel.text = currentUser?.name

        // Add custom back button
        addBackButton()
        
        // Boot and populate table with data.
        historyTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        questionsService.getQuestions(20, filter: "user_history",
            success: {
                questions in
                self.questions = questions!
                self.questionImages = self.loadThumbs(questions!)
            
                dispatch_async(dispatch_get_main_queue()) { queue in
                    self.historyTable.reloadData()
                }
    
            },
            error: { statusCode, error in
                Leaf.logError("Failed to load questions - statusCode: \(statusCode)",
                    error: error)
            }
        )
    }
    
    // Synchronously load all thumbnails.
    func loadThumbs(list: [Question]) -> [UIImage] {
        var images: [UIImage] = []
        
        for question in list {
            var err: NSError?
            let imageURL = NSURL(string: question.thumb)
            let imageData = NSData(contentsOfURL: imageURL!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
            images.append(UIImage(data: imageData!)!)
        }
        
        return images
    }
    
    /**
    * Table delegation methods
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: QuestionsTableViewCell = self.historyTable.dequeueReusableCellWithIdentifier("HistoryCell") as! QuestionsTableViewCell
        
        let positive = self.questions[indexPath.row].positive
        let negative = self.questions[indexPath.row].negative
        let neutral = self.questions[indexPath.row].neutral
        
        if positive > negative {
            cell.backgroundColor = UIColor(red: CGFloat(52/255.0), green: CGFloat(169/255.0), blue: CGFloat(72/255.0), alpha: CGFloat(0.2))
        }
        if negative > positive {
            cell.backgroundColor = UIColor(red: CGFloat(210/255.0), green: CGFloat(58/255.0), blue: CGFloat(49/255.0), alpha: CGFloat(0.1))
        }
        
        cell.questionText.text = self.questions[indexPath.row].body
        cell.Yays.text = String(self.questions[indexPath.row].positive)
        cell.Nopes.text = String(self.questions[indexPath.row].negative)
        cell.Skips.text = String(self.questions[indexPath.row].neutral)
        cell.questionImage.image = self.questionImages[indexPath.row]
        cell.questionImage.clipsToBounds = true
        var profileImageLayer = cell.questionImage.layer
        profileImageLayer.cornerRadius = 10.0
        profileImageLayer.borderWidth = 3.0
        profileImageLayer.borderColor = UIColor(red:(255/255.0), green:(255/255.0), blue:(255/255.0), alpha:0.2).CGColor
        
        return cell
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Leaf.logEvent(LeafEvents.PROFILE_QUESTION_SHOWED, withParameters: ["atIndex": indexPath.row])
        chosenQuestion = questions[indexPath.row]
        performSegueWithIdentifier("profileToShowQuestion", sender: self)
    }

    // Pass along the chosen question, to prevent a extra api call.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "profileToShowQuestion" {
            var controller = segue.destinationViewController as! ShowQuestionViewController
            controller.setQuestion(chosenQuestion!)
        }
    }
    
    func addBackButton() {
        var carouselIcon = UIImage(named: "NewQuestionNavigationIcon")
        var myBackButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        myBackButton.addTarget(self, action: "popToNewQuestion:", forControlEvents: UIControlEvents.TouchUpInside)
        myBackButton.setTitle("", forState: UIControlState.Normal)
        myBackButton.titleLabel?.font = UIFont(name: "MarkerFelt-Thin", size: 30)
        myBackButton.setBackgroundImage(carouselIcon, forState: UIControlState.Normal)
        myBackButton.tintColor = UIColor(red: CGFloat(50/255.0), green: CGFloat(50/255.0), blue: CGFloat(50/255.0), alpha: CGFloat(1.0))
        myBackButton.sizeToFit()
        var myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
    }

    func popToNewQuestion(sender:UIBarButtonItem) {
        let newQuestionViewController: UIViewController = self.storyboard!.instantiateViewControllerWithIdentifier("NewQuestionView") as! UIViewController
        self.navigationController?.popViewControllerAnimated(true)
        //(newQuestionViewController, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

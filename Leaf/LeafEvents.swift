//
//  LeafEvents.swift
//  Leaf
//
//  Created by Nichlas Lendal on 23/04/15.
//  Copyright (c) 2015 Much Fun Inc. All rights reserved.
//

import Foundation

public class LeafEvents : NSObject {
    
    public static let
    AUTH_LOGGEDIN                           = "( AUTH ) User Logged In",
    AUTH_LOGGEDOUT                          = "( AUTH ) User Logged Out",
    
    CAROUSEL_QUESTION_VOTED                 = "( CAROUSEL ) Question Voted",
    CAROUSEL_QUESTION_COMMENTSSHOWED        = "( CAROUSEL ) Comment Section Opened",
    CAROUSEL_QUESTION_COMMENTADDED          = "( CAROUSEL ) Comment Added",
    
    QUESTION_PHOTO_CAPTURED                 = "( QUESTION ) Photo Captured",
    QUESTION_PHOTO_DISCARDED                = "( QUESTION ) Photo Discarded",
    QUESTION_UPLOADED                       = "( QUESTION ) Photo & Question Uploaded",
    
    TRENDS_QUESTION_SHOWED                  = "( TRENDS ) Question Opened",
    
    PROFILE_QUESTION_SHOWED                 = "( PROFILE ) Question Opened",
    PROFILE_QUESTIONCOMMENTS_SHOWED         = "( PROFILE ) Comments Section Opened",
    PROFILE_QUESTIONCOMMENTS_COMMENTSADDED  = "( PROFILE ) Comment Added",
    
    SETTINGS_CHANGED                        = "Settings view opened - TODO"
}
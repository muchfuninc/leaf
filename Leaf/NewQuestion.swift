//
//  NewQuestion.swift
//  Leaf
//
//  Created by Nichlas Lendal on 29/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public struct NewQuestion {
    
    var
    body     : String,
    image    : UIImage?
    
    public init(body : String, image: UIImage?){
        self.body = body
        self.image = image
    }
    
    func asParams() -> [String: AnyObject] {
        return [ "question": ["body": body]]
    }
    
    func imageAsParam() -> RequestBuilder.File {
        return RequestBuilder.File(name: "image", mimeType: "image/jpeg", data: UIImageJPEGRepresentation(image, 1.0))
    }
    
}
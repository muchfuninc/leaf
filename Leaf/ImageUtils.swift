//
//  ImageUtils.swift
//  Leaf
//
//  Created by Nichlas Lendal on 05/11/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

class ImageUtils: NSObject {
    
    class func cropImageByView(image: UIImage, view: UIView) -> UIImage! {
        var scale = UIScreen.mainScreen().scale
        var maxPositionY = UIScreen.mainScreen().bounds.size.height
        
        // original size in pixels
        var originalWidth  = image.size.width
        var originalHeight = image.size.height
        
        var viewPositionY = view.superview?.convertPoint(view.layer.frame.origin, toView: nil).y
        var posY = (originalHeight / maxPositionY) * viewPositionY! + CGFloat(28)
        var posX = CGFloat(0)
        
        var cropSquare = CGRectMake(
            posY * scale,
            posX * scale,
            originalWidth,
            originalWidth + posY)
        
        var imageRef = CGImageCreateWithImageInRect(image.CGImage, cropSquare);
        return UIImage(CGImage: imageRef, scale: UIScreen.mainScreen().scale, orientation: image.imageOrientation)
    }
    
    class func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.mainScreen().scale)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}
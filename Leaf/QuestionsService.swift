//
//  QuestionsService.swift
//  Leaf
//
//  Created by Nichlas Lendal on 26/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

public var questionsService: QuestionsService = QuestionsService(FinkAPI());

public class QuestionsService: NSObject {
    
    let finkAPI : FinkAPI
    
    init(_ finkAPI: FinkAPI) {
        self.finkAPI = finkAPI
    }
    
    public func getQuestions(limit: Int, filter: String?, success: (questions: [Question]?) -> (), error: (statusCode: Int, error: NSError?) -> ()) {
        RequestBuilder
            .GET(finkAPI.questions())
            .withParams(["limit": limit, "filter": filter!])
            .onSuccess({ data in
                success(questions: Questions(JSONDecoder(data)).questions )
            })
            .onError(error, queue: NSOperationQueue.mainQueue())
            .execute()
    }
    
    public func newQuestion(question: NewQuestion, success: () -> Void, error: (statusCode: Int, error: NSError?) -> Void){
        RequestBuilder
            .POST(finkAPI.questions())
            .withParams(question.asParams())
            .withFile(question.imageAsParam())
            .onSuccess({ data in success() } )
            .onError(error, queue: NSOperationQueue.mainQueue())
            .execute()
    }
    
}

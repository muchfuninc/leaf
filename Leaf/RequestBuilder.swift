//
//  RequestBuilder.swift
//  Leaf
//
//  Created by Nichlas Lendal on 26/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

/**
 * RequestBuilder til at opbygge et request mod en URL.
 * Understøtter GET & POST Http kald.
 * metoden execute() kaldes når http kaldet skal udføres.
 *
 * Ved GET kald sættes parametre som body data på requestet.
 * Ved POST bliver parametre som default serialiseret til JSON.
 * Hvis der eksisterer en fil bliver requested til multipart/form-data
 * - Parametre vil her fortsat være json.
 */
public class RequestBuilder: NSObject {
    
    public class File: NSObject {
        
        var
        name     : String?,
        mimeType : String?,
        data     : NSData?

        init(name: String?, mimeType: String?, data: NSData?){
            self.name = name
            self.mimeType = mimeType
            self.data = data
        }
        
    }

    private var
    successHandler : SuccessHandler!,
    errorHandler   : ErrorHandler!,
    params  : Dictionary<String, AnyObject>?,
    file    : File?
    
    private let
    session : NSURLSession!,
    request : NSMutableURLRequest
    
    private init(_ request: NSMutableURLRequest){
        self.request = request
        self.session = NSURLSession.sharedSession()
    }

    public class func GET(url: String) -> RequestBuilder  {
        let request = buildNSMutableURLRequest("GET", url)
        return RequestBuilder(request)
    }

    public class func POST(url: String) -> RequestBuilder {
        let request = buildNSMutableURLRequest("POST", url)
        return RequestBuilder(request)
    }

    public func withParams(params: Dictionary<String, AnyObject>) -> RequestBuilder {
        self.params = params
        return self
    }
    
    public func withFile(file: File) -> RequestBuilder {
        self.file = file
        return self
    }
    
    public func onSuccess(closure: (data: AnyObject) -> (), queue: NSOperationQueue? = nil) -> RequestBuilder {
        self.successHandler = SuccessHandler(closure: closure, queue: queue)
        return self
    }

    public func onError(closure: (statusCode: Int, error: NSError?) -> (), queue: NSOperationQueue? = nil) -> RequestBuilder {
        self.errorHandler = ErrorHandler(closure: closure, queue: queue)
        return self
    }

    func execute()
    {
        if currentUser != nil {
            request.addValue("Bearer \(currentUser!.apiKey!)", forHTTPHeaderField: "Authorization")
        }

        switch request.HTTPMethod {
            case "GET"  : executeGET()
            case "POST" : executePOST()
            default : fatalError("HTTPMetoden " + request.HTTPMethod + " er ikke understøttet")
        }
    }

    private func executeGET() {
        if params?.count > 0 {
            //build url with query params
            var originalURL : String! = request.URL!.absoluteString! as String
            var queryString : String! = self.createQueryString(self.params!)
            var newURL      : NSURL! = NSURL(string: originalURL + queryString)
            
            //override URL in NSMutableRequestURL
            request.URL = newURL
        }
        
        //headers
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        session.dataTaskWithRequest(request) {
            data, response, error in
                //NSString(data: data, encoding: NSUTF8StringEncoding)
                self.handleResult(data, error, self.getStatusCode(response))
        }.resume()
    }
    
    private func executePOST(){
        var requestBody : NSData?
        var contentType : String?

        if file != nil {
            contentType = "multipart/form-data; boundary=FinkFormBoundary"
            requestBody = self.createMultipartRequestBody()
        } else if params?.count >= 0 {
            contentType = "application/json"
            requestBody = self.createJSONRequestBody()
        }
        
        //headers
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        session.uploadTaskWithRequest(request, fromData: requestBody) {
            data, response, error in
                //NSString(data: data, encoding: NSUTF8StringEncoding)
                self.handleResult(data, error, self.getStatusCode(response))
        }.resume()
    }
    
    private class SuccessHandler {
        private var
        closure : (data: AnyObject) -> (),
        queue   : NSOperationQueue?

        init(closure: (data: AnyObject) -> (), queue: NSOperationQueue? = nil){
            self.closure = closure
            self.queue = queue
        }
    }

    private class ErrorHandler {
        private var
        closure: (statusCode: Int, error: NSError?) -> (),
        queue:   NSOperationQueue?

        init(closure: (statusCode: Int, error: NSError?) -> (), queue: NSOperationQueue? = nil){
            self.closure = closure
            self.queue = queue
        }
    }

    private func handleResult(data: AnyObject?, _ error: NSError?, _ statusCode: Int)
    {
        if data != nil
        {
            let handler  = successHandler!
            let success  = { handler.closure(data: data!) }
            if let queue = handler.queue { queue.addOperationWithBlock(success) }
            else { success() }
        }
        else
        {
            let handler  = errorHandler!
            let failure  = { handler.closure(statusCode: statusCode, error: error) }
            if let queue = handler.queue { queue.addOperationWithBlock(failure) }
            else { failure() }
        }

        errorHandler = nil
        successHandler = nil
    }

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // Private Helper Functions
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    private class func buildNSMutableURLRequest(httpMethod: String,_ url: String) -> NSMutableURLRequest {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = httpMethod
        return request
    }
    
    /* Laver NSData til brug i request body - Bruges til POST requests hvor der ikke skal uploades fil */
    private func createJSONRequestBody() -> NSData! {
        if params?.count >= 0 {
            if NSJSONSerialization.isValidJSONObject(self.params!) {
                var options = NSJSONWritingOptions.PrettyPrinted
                if let json = NSJSONSerialization.dataWithJSONObject(self.params!, options: options, error: nil) {
                    return json
                }
            }
            NSException(name: "Parsing error", reason: "Could not parse user data", userInfo: nil).raise()
        }

        return nil
    }
    
    /* Laver NSData indeholde et multipart request til brug i request body - Bruges til POST requests hvor der eksisterer en fil */
    private func createMultipartRequestBody() -> NSData! {
        var body = NSMutableData()
        var boundary = "FinkFormBoundary"

        if params?.count > 0 {
            //Body part for json data
            body.appendData(self.asData("--%@\r\n", boundary))
            body.appendData(self.asData("Content-Disposition: form-data; name=\"%@\"\r\n", "json"))
            body.appendData(self.asData("%@", "Content-Type: application/json\r\n\r\n"))
            body.appendData(self.asData("%@", "\r\n"))
            body.appendData(createJSONRequestBody()!)
        }
        
        //Body part for the file
        body.appendData(self.asData("--%@\r\n", boundary))
        body.appendData(self.asData("Content-Disposition: form-data; name=\"%@\"; filename=\""+file!.name!+"\"\r\n", "file"))
        body.appendData(self.asData("%@", "Content-Type: " + file!.mimeType! + "\r\n\r\n"))
        body.appendData(file!.data!)
        
        //end
        body.appendData(self.asData("\r\n\r\n--%@--\r\n", boundary))
        
        return body
    }
    
    /* Laver en query string - Bruges til GET requests */
    private func createQueryString(parameters: [String:AnyObject]) -> String {
        var urlVars = [String]()
        for (k, var v) in parameters {
            //v =v.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            urlVars += [k + "=" + "\(v)"]
        }
        return (!urlVars.isEmpty ? "?" : "") + join("&", urlVars)
    }
    
    private func getStatusCode(response: AnyObject?) -> Int {
        if let httpResponse = response as? NSHTTPURLResponse
        {
            return httpResponse.statusCode
        }
        return 0 //kunne ikke udlede statuskode.
    }
    
    private func asData(format: String, _ text: String) -> NSData {
        var formatted : NSString = NSString(format: format, text) as String
        var asData = formatted.dataUsingEncoding(NSUTF8StringEncoding)
        return asData!
    }
    
}

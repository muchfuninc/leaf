//
//  QuestionImageView.swift
//  ask
//
//  Created by Martin Elvar on 22/09/14.
//  Copyright (c) 2014 Martin Elvar. All rights reserved.
//

import UIKit
import Foundation

class QuestionImageView: UIImageView {

    var lastLocation: CGPoint = CGPointMake(0, 0)
    var originalLocation: CGPoint?
    var questionQueue: QuestionQueue?
    var questionImages:Array<UIImage> = []

    var onPositive: ()->() = {}
    var onNegative: ()->() = {}
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Save the original position of our view.
        originalLocation = self.frame.origin
        
        var panRecognizer = UIPanGestureRecognizer(target: self, action: "detectPan:")
        self.gestureRecognizers = [panRecognizer]

        setSelfTheming()
    }

    func setQuestionQueue(questionQueue: QuestionQueue) {
        self.questionQueue = questionQueue

        // Load and set the first image.
        self.setValue(self.questionQueue?.getCurrentImage(), forKey: "image")

        // Load and set next image, when next question is triggered.
        self.questionQueue?.onNext({ () -> () in
            self.setValue(self.questionQueue?.getCurrentImage(), forKey: "image")
        })
    }

    @IBAction func detectPan(recognizer: UIPanGestureRecognizer) {

        if recognizer.state == UIGestureRecognizerState.Ended {
            var imageViewCenter = CGPoint(x: self.originalLocation!.x + self.frame.size.width/2, y: self.originalLocation!.y + self.frame.size.height/2 )
            // + 200 for easiest swipe
            if self.center.x < (self.originalLocation!.x + (self.frame.size.width / 2))  {
                UIView.animateWithDuration(0.2, animations: {
                    self.center = CGPoint(x: self.originalLocation!.x + self.frame.size.width/2 - 500, y: self.originalLocation!.y + self.frame.size.height/2)
                    }, completion: { finished in
                        self.center = imageViewCenter
                        self.animateImage(self.questionQueue!.getCurrentImage())
                        self.onNegative()
                    }
                )
            // - 100 for easiest swipe
            } else if self.center.x > (self.originalLocation!.x + self.frame.size.width - (self.frame.size.width / 2 )) {
                UIView.animateWithDuration(0.2, animations: {
                    self.center = CGPoint(x: self.originalLocation!.x + self.frame.size.width + 500, y: self.originalLocation!.y + self.frame.size.height/2)
                    }, completion: { finished in
                        self.center = imageViewCenter
                        self.animateImage(self.questionQueue!.getCurrentImage())
                        self.onPositive()
                    }
                )
            } else {
                UIView.animateWithDuration(0.4, animations: {
                    self.center = imageViewCenter
                    }, completion: { finished in
                        Leaf.logDebug("SNAP BACK!")
                    }
                )
            }

            return
        }

        var translation = recognizer.translationInView(self.superview!)
        self.center = CGPointMake(lastLocation.x + translation.x, self.originalLocation!.y + self.frame.size.height/2)
    }
    
    func setSelfTheming () {
        self.clipsToBounds = true
        self.layer.cornerRadius = 12.0
        self.layer.borderWidth = 3.0
        self.layer.borderColor = UIColor(red:(180/255.0), green:(180/255.0), blue:(180/255.0), alpha:0.1).CGColor
    }
    
    func animateImage(image:UIImage) {
        var image:UIImage = image
        let t:NSTimeInterval = 1;
        let t1:NSTimeInterval = 0;
        self.alpha = 0.1
        self.setValue(image, forKey: "image")
        
        UIView.animateWithDuration(1.0, delay: 0.0, options:UIViewAnimationOptions.CurveEaseOut, animations: {() in
            self.alpha = 1.0;
            },
            completion: {(Bool) in
        })
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.superview!.bringSubviewToFront(self.superview!)
        lastLocation = self.center
    }
}

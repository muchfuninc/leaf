//
//  VoteService.swift
//  Leaf
//
//  Created by Martin Elvar on 31/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

let voteService = VoteService(FinkAPI())

class VoteService {
    let finkAPI : FinkAPI
    
    init(_ finkAPI: FinkAPI){
        self.finkAPI = finkAPI
    }

    func createVote(vote: Vote, fail: (statusCode: Int, error: NSError?)->()) {
        RequestBuilder
            .POST(finkAPI.votes())
            .withParams(vote.asParams())
            .onSuccess( { data in
                // No need to handle success.
            })
            .onError({ statusCode, error in
                fail(statusCode: statusCode, error: error)
            })
            .execute()
    }
}
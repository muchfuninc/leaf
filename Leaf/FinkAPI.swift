//
//  API.swift
//  Leaf
//
//  Created by Nichlas Lendal on 29/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

class FinkAPI {
    
    let
    api_questions = "questions",
    api_users = "users",
    api_votes = "votes",
    json_ext = ".json"
    
    func questions() -> String {
        return config.api_url + api_questions + json_ext
    }
    
    func users() -> String {
        return config.api_url + api_users + json_ext
    }

    func votes() -> String {
        return config.api_url + api_votes + json_ext
    }

    func userById(id: String) -> String {
        return config.api_url + api_questions + "/\(id)" + json_ext
    }
}

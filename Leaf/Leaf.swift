//
//  Leaf.swift
//  Leaf
//
//  Created by Nichlas Lendal on 24/04/15.
//  Copyright (c) 2015 Much Fun Inc. All rights reserved.
//

import Foundation

public enum LogLevel: Int {
    case TRACE
    case DEBUG
    case INFO
    case EVENT
    case WARN
    case ERROR
    case NONE
    
    public func description() -> String {
        switch self {
        case .TRACE:
            return "Trace"
        case .DEBUG:
            return "Debug"
        case .INFO:
            return "Info"
        case .EVENT:
            return "Event"
        case .WARN:
            return "Warn"
        case .ERROR:
            return "Error"
        case .NONE:
            return "None"
        }
    }
}

public class Leaf {
    public static var dateFormatter : NSDateFormatter = NSDateFormatter()
    init(){}
    
    public static func logWarn(message: String!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        self.log(LogLevel.WARN, message: message, method: method, file: clazz, line: line)
    }
    
    public static func logInfo(message: String!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        self.log(LogLevel.INFO, message: message, method: method, file: clazz, line: line)
    }
    
    public static func logDebug(message: String!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        self.log(LogLevel.DEBUG, message: message, method: method, file: clazz, line: line)
    }
    
    public static func logTrace(message: String!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        self.log(LogLevel.TRACE, message: message, method: method, file: clazz, line: line)
    }
    
    public static func logEvent(eventName: String!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        Flurry.logEvent(eventName)
        self.log(LogLevel.EVENT, message: eventName, method: method, file: clazz, line: line)
    }
    
    public static func logEvent(eventName: String!, timed: Bool, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        Flurry.logEvent(eventName, timed: timed)
        self.log(LogLevel.EVENT, message: eventName + " timed: \(timed)", method: method, file: clazz, line: line)
    }
    
    public static func logEvent(eventName: String!, withParameters: [NSObject : AnyObject]!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        Flurry.logEvent(eventName, withParameters: withParameters)
        self.log(LogLevel.EVENT, message: eventName + " parameters: \(self.printDictionary(withParameters))", method: method, file: clazz, line: line)
    }
    
    public static func logEvent(eventName: String!, withParameters: [NSObject : AnyObject]!, timed: Bool, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        Flurry.logEvent(eventName, withParameters: withParameters, timed: timed)
        self.log(LogLevel.EVENT, message: eventName + " parameters: \(self.printDictionary(withParameters))" + " timed: \(timed)", method: method, file: clazz, line: line)
    }

    public static func logError(message: String!, error: NSError!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        Flurry.logError("[\(clazz.lastPathComponent):\(String(line))]", message: message, error: error)
        self.log(LogLevel.ERROR, message: message + "\n error: \(error)", method: method, file: clazz, line: line)
    }
    
    public static func logError(message: String!, exception: NSException!, method : String = __FUNCTION__, clazz : String = __FILE__, line : Int = __LINE__){
        Flurry.logError("[\(clazz.lastPathComponent):\(String(line))]", message: message, exception: exception)
        self.log(LogLevel.ERROR, message: message + "\n error: \(exception)", method: method, file: clazz, line: line)
    }
    
    private static func log(logLevel : LogLevel, message: String!, method : String!, file : String!, line : Int!){
        if ( logLevel.rawValue >= config.logLevel.rawValue ){
            var logMessage : String = ""
            
            //Date.
            dateFormatter.locale = NSLocale.currentLocale()
            dateFormatter.dateFormat = config.logDateFormat
            logMessage += dateFormatter.stringFromDate(NSDate())
            
            //Log level
            logMessage += " [\(logLevel.description())]"
            
            //Thread
            var threadName : String = (NSThread.isMainThread() ? "main" : NSThread.currentThread().name)
            if (threadName == ""){
                threadName = String(format:"%p", NSThread.currentThread())
            }
            logMessage += " [\(threadName)]"
            
            //Class + Linenumber
            logMessage += " [\(file.lastPathComponent):\(String(line))]"
            
            //method
            logMessage += " \(method):"
            
            //Message
            logMessage += " \(message)"
            println(logMessage)
        }
    }
    
    private static func printDictionary(dictionary : [NSObject : AnyObject]! ) -> String {
        var prettyString : String = "["
        var counter : Int = 1
        for (key, value) in dictionary {
            prettyString += "\(key): \(value)"     //String(key as! NSString) + ": " + String(value as! NSString)
            if (dictionary.count != counter){
                prettyString += ", "
            }
            counter = counter + 1
        }
        
        return prettyString + "]"
    }

}
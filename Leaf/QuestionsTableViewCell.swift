//
//  QuestionsTableViewCell.swift
//  Leaf
//
//  Created by David Hammer on 31/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import UIKit

class QuestionsTableViewCell: UITableViewCell {

    @IBOutlet var questionText: UILabel!
    @IBOutlet var Yays: UILabel!
    @IBOutlet var Nopes: UILabel!
    @IBOutlet var Skips: UILabel!
    @IBOutlet var questionNumber: UILabel!
    @IBOutlet var questionImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

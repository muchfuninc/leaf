//
//  LibraryController.swift
//  Leaf
//
//  Created by Nichlas Lendal on 04/11/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

import UIKit
import AVFoundation

/**

        Denne class er pt. ikke i brug, da vi har fravalgt at bruger kan vaelge billeder fra library.

*/


class LibraryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var pickedImage : UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var topLayerView: UIView!
    @IBOutlet weak var draggableAreaView: DraggableView!
    @IBOutlet weak var bottomLayerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //FIXME: man kommer tilbage fra create.
        if pickedImage == nil {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        
        setupDraggableArea()
        if pickedImage != nil {
            imageView.image = pickedImage
        }
    }
    
    func setupDraggableArea(){
        draggableAreaView.topFollower = topLayerView
        draggableAreaView.bottomFollower = bottomLayerView
        
        draggableAreaView.layer.borderColor = UIColor(red: CGFloat(50/255.0), green: CGFloat(50/255.0), blue: CGFloat(50/255.0), alpha: CGFloat(1.0)).CGColor
        draggableAreaView.layer.borderWidth = CGFloat(3.0)
    }
    
    @IBAction func pickArea(sender: AnyObject) {
        var scaledImage =  ImageUtils.cropImageByView(pickedImage!, view: self.draggableAreaView)
        //FIXME: billedet skal kun scaleres hvis det er over 800x800
        //var sizedImage = ImageUtils.resizeImage(scaledImage, targetSize: CGSize(width: 800, height: 800))
        var sizedImage = scaledImage
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("CreateQuestionView") as! CreateQuestionViewController
        controller.injectedImage = sizedImage
        self.presentViewController(controller, animated: false, completion: nil)
    }
    
    
    @IBAction func dismissView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

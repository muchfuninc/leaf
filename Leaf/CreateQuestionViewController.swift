import UIKit
import AVFoundation

class CreateQuestionViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var questionTextField: UITextField!
    @IBOutlet var navigationBar: UINavigationItem?
    
    //image der bliver injected fra NewQuestionViewController.
    var injectedImage : UIImage?
    var redirectCallback : ()->() = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if injectedImage != nil {
            imageView?.setValue(injectedImage, forKey: "image")
        } else {
            Leaf.logError("No UIImage injected, who redirected here?",
                error: nil)
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        setImageHolderViewTheming()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func setImageHolderViewTheming () {
        imageView?.clipsToBounds = true
        imageView?.layer.cornerRadius = 12.0
        imageView?.layer.borderWidth = 5.0
        imageView?.layer.borderColor = UIColor(red:(103/255.0), green:(136/255.0), blue:(172/255.0), alpha:0.2).CGColor
    }
    
    @IBAction func didClickBack(sender: UIButton) {
        Leaf.logEvent(LeafEvents.QUESTION_PHOTO_DISCARDED)
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func createQuestion(sender: AnyObject) {
        var newQuestion = NewQuestion(body: questionTextField.text, image: injectedImage!)
        
        questionsService.newQuestion(newQuestion,
            success: {
                Leaf.logEvent(LeafEvents.QUESTION_UPLOADED, withParameters: ["questionLenght": count(newQuestion.body)])
                
                // Give the initial controller, a change to redirect further.
                self.dismissViewControllerAnimated(true, completion: nil)
                self.redirectCallback()
            },
            error: { statusCode, error in
                Leaf.logError("Failed uploading question - statusCode: \(statusCode)",
                    error: error)
            }
        )
        
    }
    
}

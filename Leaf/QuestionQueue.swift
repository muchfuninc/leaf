//
//  QuestionQueue.swift
//  Leaf
//
//  Created by Martin Elvar on 27/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

class QuestionQueue {
    var queue: Array<Question> = []
    var imageDataQueue: Array<NSData> = []

    // Hook subscribers.
    var onReadyObservers: Array<()->Void> = []
    var onNextObservers: Array<()->Void> = []
    var onLoadObservers: Array<()->Void> = []
    var onEmptyObservers: Array<()->Void> = []

    func initializeQueue() {
        if config.runOnline {
           initializeLiveQueue()
        } else {
           initializeStubQueue()
        }
    }
    
    private func initializeLiveQueue(){
        // Fetch initial batch.
        fetchNextBatch(true)
    }
    
    private func initializeStubQueue(){
        // Enable stubs.
        var questions = QuestionsStubService().getQuestions()
        
        // Load the images for the new batch.
        self.loadImages(questions)
        // Put the batch on the queue.
        self.queue += questions
        
        // Let all subscribers do there job.
        for closure in self.onReadyObservers {
            closure()
        }
    }

    func getCurrentQuestion() -> Question {
        return self.queue.first!
    }

    func getCurrentImage() -> UIImage {

        if let imageData = imageDataQueue.first {
            return UIImage(data: imageDataQueue.first!)!
        }

        // If the image wasn't yet loaded, fallback manually.
        var err: NSError?
        let imageURL = NSURL(string: queue.first!.image)
        let imageData = NSData(contentsOfURL: imageURL!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
        return UIImage(data: imageData!)!
    }

    func loadImages(questions: Array<Question>) {
        // Load the images nonblocking.
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            var err: NSError?

            for question in questions {
                let imageURL = NSURL(string: question.image)
                let imageData = NSData(contentsOfURL: imageURL!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
                self.imageDataQueue.append(imageData!)

            }
        })
    }

    func next() {
        // Remove the previous question
        queue.removeAtIndex(0)
        imageDataQueue.removeAtIndex(0)
        
        Leaf.logDebug("\(queue.count) questions left")

        // If there is no questions remaining, fetch next batch.
        if queue.count == 0 {
            fetchNextBatch(false)
            
            // return, so the next hook is not triggered until the batch is ready.
            return
        }

        // Let all subscribers do there job.
        for closure in onNextObservers {
            closure()
        }
    }
    
    func fetchNextBatch(initialLoad: Bool) {
        questionsService.getQuestions(10, filter: "random",
            success: { questions in
                // Uncomment to enable stubs.
                //var questions = QuestionsStubService().getQuestions()
            
                // if there was no questions in the batch, trigger empty hook.
                if questions!.count == 0 {
                    for closure in self.onEmptyObservers {
                        closure()
                    }
                
                    // return as there are no questions to append to the queue.
                    return
                }

                // Load the images for the new batch.
                self.loadImages(questions!)
                // Put the batch on the queue.
                self.queue += questions!

                // Let all subscribers know that the batch is ready.
                for closure in self.onNextObservers {
                    closure()
                }
            
                // If this was the initial batch fetch, trigger Ready hook.
                if initialLoad {
                    for closure in self.onReadyObservers {
                        closure()
                    }
                }
            },
            error: { statusCode, error in
                Leaf.logError("Failed to pushVote - statusCode: \(statusCode)",
                    error: error)
            }
        )
    }
    
    /**
    * Queue hooks
    */
    
    // Called when the first batch is loaded.
    func onReady(closure: () -> ()) {
        self.onReadyObservers.append(closure)
    }

    // Called when next question is set.
    func onNext(closure: () -> ()) {
        self.onNextObservers.append(closure)
    }
    
    // Called when new batch is being loaded.
    func onLoad(closure: () -> ()) {
        self.onLoadObservers.append(closure)
    }

    // Called when the queue is empty for questions, and could not fetch anymore from the api.
    func onEmpty(closure: () -> ()) {
        self.onEmptyObservers.append(closure)
    }

}

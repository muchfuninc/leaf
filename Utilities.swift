//
//  Utilities.swift
//  Leaf
//
//  Created by Martin Elvar on 30/10/14.
//  Copyright (c) 2014 Much Fun Inc. All rights reserved.
//

import Foundation

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

func loadExternalImage(url: String) -> UIImage {
    var err: NSError?
    var imageData = NSData(contentsOfURL: NSURL(string: url)!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)

    return UIImage(data: imageData!)!
}
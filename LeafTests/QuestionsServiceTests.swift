//
//  QuestionsServiceTests.swift
//  Leaf
//
//  Created by Nichlas Lendal on 02/04/15.
//  Copyright (c) 2015 Much Fun Inc. All rights reserved.
//

import Leaf
import UIKit
import XCTest

class QuestionsServiceTests: XCTestCase {
    
    func test_CreateQuestion_simple() {
        var succeeded : Bool = false
    
        var question = NewQuestion(body: "Et spørgsmål!", image: UIImage(named: "doge.png"))
        
        questionsService.newQuestion(question,
            success: {
                succeeded = true
            },
            error: { statusCode, error in
                println("test_CreateQuestion_simple: failed HTTP Request : HTTP status code \(statusCode) Error: \(error)")
        })
        
        XCTAssertEqual(succeeded, false)
    }
    
    func test_GetQuestions_WithLimit(){
        var succeeded : Bool = false
        
        questionsService.getQuestions(10, filter: "random",
            success: { questions in
                var hello = questions as Array<Question>!
                if hello.count == 2 {
                    succeeded = true
                }
            },
            error: { statusCode, error in
                println("test_GetQuestions_WithLimit: failed HTTP Request : HTTP status code \(statusCode) Error: \(error)")
            }
        )
        
        XCTAssertEqual(succeeded, false)
    }
    
}
